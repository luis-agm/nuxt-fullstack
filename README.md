# Jojoto Playground

Just a small Nuxt site with a Koa server. It's deployed to Heroku as a universal site, and to Netlify (No API).

>"Jojoto" means :corn:.

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# Analyze bundle
$ npm run analyze

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```