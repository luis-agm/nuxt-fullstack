const throttle = (fn, wait) => {
  let time = Date.now()
  return function () {
    if (time + wait - Date.now() < 0) {
      fn()
      time = Date.now()
    }
  }
}

const debounce = (fn, time) => {
  let timeoutId
  return wrapper
  function wrapper(...args) {
    if (timeoutId) {
      clearTimeout(timeoutId)
    }
    timeoutId = setTimeout(() => {
      timeoutId = null
      fn(...args)
    }, time)
  }
}

const sortBy = (arr, key) => {
  return arr
    .concat()
    .sort((a, b) => (a[key] > b[key] ? 1 : b[key] > a[key] ? -1 : 0))
}

export { throttle, debounce, sortBy }
