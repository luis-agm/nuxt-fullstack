import Router from 'koa-router'

const router = new Router()

router.get('/api/ping', ctx => {
  ctx.body = 'pong'

  if (
    ctx.request.headers &&
    ctx.request.headers['user-agent'] &&
    !ctx.request.headers['user-agent'].includes('curl')
  ) {
    ctx.body = '🌽'
  }
})

router.get('/api/popover/:id', async ctx => {
  const data = await new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        text: `This text comes fron the API, customized for popover with id: `,
        id: ctx.params.id
      })
    }, 1000)
  })

  ctx.body = data
})

export default router
